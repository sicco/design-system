module.exports = {
  stories: [
    '../src/getting-started.stories.mdx',
    '../src/guidelines/spacing.stories.mdx',
    '../src/guidelines/icons.stories.mdx',
    '../src/guidelines/voice-and-tone.stories.mdx',
    '../src/components/**/*.stories.js',
  ],
  addons: [
    'storybook-addon-styled-component-theme/dist/register',
    '@storybook/preset-create-react-app',
    '@storybook/addon-docs',
    '@storybook/addon-viewport/register',
  ],
}
