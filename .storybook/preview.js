import React from 'react'
import {INITIAL_VIEWPORTS} from '@storybook/addon-viewport'
import {ThemesProvider} from 'storybook-addon-styled-component-theme'
import {darkTheme, defaultTheme} from '../src/themes'
import {GlobalStyles} from '../src'

export const decorators = [(Story) =>
  <ThemesProvider themes={[defaultTheme, darkTheme]}>
    <>
      <GlobalStyles/>
      <Story/>
    </>
  </ThemesProvider>];

export const parameters = {
  viewport: {
    viewports: INITIAL_VIEWPORTS,
  },
};