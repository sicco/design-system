## [13.0.1](https://gitlab.com/commonground/core/design-system/compare/v13.0.0...v13.0.1) (2020-09-07)


### Bug Fixes

* relax peerDependencies to allow patches ([6ce7df9](https://gitlab.com/commonground/core/design-system/commit/6ce7df96b6b1b60ad9ba672f1c749d8732331e05))

# [13.0.0](https://gitlab.com/commonground/core/design-system/compare/v12.6.0...v13.0.0) (2020-09-03)


### Features

* **toaster:** reposition Toasts bottom/right ([f520098](https://gitlab.com/commonground/core/design-system/commit/f5200988d1cf91ab815816becbe0cca7d8e1b61e))
* **toaster:** teasers are now removed after 5s timeout ([ec8a3bf](https://gitlab.com/commonground/core/design-system/commit/ec8a3bfe67084de05acae12cc243cd6f817350c0))


### BREAKING CHANGES

* **toaster:** `position` prop is removed.

# [12.6.0](https://gitlab.com/commonground/core/design-system/compare/v12.5.0...v12.6.0) (2020-09-03)


### Features

* **drawer:** add option to skip opening animation ([fad814e](https://gitlab.com/commonground/core/design-system/commit/fad814eb1c96ba3905355e87f0b5862931ca8b16))

# [12.5.0](https://gitlab.com/commonground/core/design-system/compare/v12.4.3...v12.5.0) (2020-06-19)


### Features

* **collapsible:** add aria attributes ([de22af7](https://gitlab.com/commonground/core/design-system/commit/de22af701a34e74b21496a47ef6b201babb50903))

## [12.4.3](https://gitlab.com/commonground/core/design-system/compare/v12.4.2...v12.4.3) (2020-06-11)


### Bug Fixes

* **button:** fix contrast on dark theme ([de9e8f7](https://gitlab.com/commonground/core/design-system/commit/de9e8f7b15b9498084165f1d5bfa5a8e7cb34d44))

## [12.4.2](https://gitlab.com/commonground/core/design-system/compare/v12.4.1...v12.4.2) (2020-06-09)


### Bug Fixes

* drawer content scrolling and bg positioning ([165a071](https://gitlab.com/commonground/core/design-system/commit/165a071cc715b3baf35601ea966888d7f14ad209))

## [12.4.1](https://gitlab.com/commonground/core/design-system/compare/v12.4.0...v12.4.1) (2020-06-09)


### Bug Fixes

* **button:** secondary color ([db2bf84](https://gitlab.com/commonground/core/design-system/commit/db2bf84c4f073d3536e05986737d4a1e7e43c347))

# [12.4.0](https://gitlab.com/commonground/core/design-system/compare/v12.3.1...v12.4.0) (2020-06-05)


### Features

* **collapsible:** introduce component ([3daf60c](https://gitlab.com/commonground/core/design-system/commit/3daf60c24372d17ed35649134c51c4d4f04ac4fe)), closes [#8](https://gitlab.com/commonground/core/design-system/issues/8)
* **iconflippingchevron:** introduce component ([12867f2](https://gitlab.com/commonground/core/design-system/commit/12867f25568ac4ba9cacaa063b886037b5811552)), closes [#8](https://gitlab.com/commonground/core/design-system/issues/8)

## [12.3.1](https://gitlab.com/commonground/core/design-system/compare/v12.3.0...v12.3.1) (2020-06-02)


### Bug Fixes

* removes deprecation warning during build ([7cf79c2](https://gitlab.com/commonground/core/design-system/commit/7cf79c214140c143fa71f6b202fc24f80ffede7d))

# [12.3.0](https://gitlab.com/commonground/core/design-system/compare/v12.2.0...v12.3.0) (2020-06-02)


### Bug Fixes

* temp test for review app ([5a95396](https://gitlab.com/commonground/core/design-system/commit/5a953961ca79ee694210faa0bccc4c2816b4eaf2))
* toast component was not exported ([196f129](https://gitlab.com/commonground/core/design-system/commit/196f12962402b3ee89d789f414f878f1d9b1bb52))


### Features

* added Toast component and some tweaks ([d677096](https://gitlab.com/commonground/core/design-system/commit/d6770962f42cb8f4b78c182694081638af4fb600))
* toaster component ([e09c5c0](https://gitlab.com/commonground/core/design-system/commit/e09c5c029a9fce756026970e92943d3880864ebc))

# [12.2.0](https://gitlab.com/commonground/core/design-system/compare/v12.1.2...v12.2.0) (2020-05-29)


### Features

* **table:** add horizontal padding to cells ([dd9be3d](https://gitlab.com/commonground/core/design-system/commit/dd9be3da03891fdb9432360387bed7de4b0af853))
* **table:** introduce component ([e098765](https://gitlab.com/commonground/core/design-system/commit/e098765f0125a639498000fe0ae6fe053783fad8))

## [12.1.2](https://gitlab.com/commonground/core/design-system/compare/v12.1.1...v12.1.2) (2020-04-28)


### Bug Fixes

* **drawer:** disable focusOn when using noMask ([6f3157a](https://gitlab.com/commonground/core/design-system/commit/6f3157a41c5780ee87eb029ec10dfe126b28e1ba))

## [12.1.1](https://gitlab.com/commonground/core/design-system/compare/v12.1.0...v12.1.1) (2020-04-24)


### Bug Fixes

* **drawer:** make sure props for Header are being passed on ([3a98afc](https://gitlab.com/commonground/core/design-system/commit/3a98afcb107cd3a0f6b098d02faffd2790a6f396))

# [12.1.0](https://gitlab.com/commonground/core/design-system/compare/v12.0.0...v12.1.0) (2020-04-24)


### Features

* **drawer:** add noMask property to disable mask ([8613376](https://gitlab.com/commonground/core/design-system/commit/86133765eb309f9481744c38b31ba6e4c5fbd3f9)), closes [commonground/nlx/nlx#923](https://gitlab.com/commonground/nlx/nlx/issues/923)

# [12.0.0](https://gitlab.com/commonground/core/design-system/compare/v11.2.1...v12.0.0) (2020-04-24)


### Features

* **drawer:** improve default ui ([7959abd](https://gitlab.com/commonground/core/design-system/commit/7959abd7544b3dfed35f976497cb4613bd7c8447)), closes [commonground/nlx/nlx#923](https://gitlab.com/commonground/nlx/nlx/issues/923)


### BREAKING CHANGES

* **drawer:** When using the Drawer, you should provide both the header and content section manually. We've provided the `Drawer.Header` and `Drawer.Content` for this.

If you do have the need for customizing the header or content, you can provide your own components as children to the Drawer.

## [11.2.1](https://gitlab.com/commonground/core/design-system/compare/v11.2.0...v11.2.1) (2020-04-24)


### Bug Fixes

* add styling for disabled form fields ([55f57a3](https://gitlab.com/commonground/core/design-system/commit/55f57a338c51c9ddbbc4c0b058ca36533916d6e5))
* checkbox color was not applied correctly ([0d807c5](https://gitlab.com/commonground/core/design-system/commit/0d807c59115300244cff6cbdb36f938a81c76811))
* improve checkbox focus style ([7bc15a1](https://gitlab.com/commonground/core/design-system/commit/7bc15a13ed1c40d0c41d5ca9f8603cb562ebfe1b))
* improve ui for radio button groups ([011e0d0](https://gitlab.com/commonground/core/design-system/commit/011e0d05ffe5a389837e79b8328c94d201cf05f1))
* update text color for labels ([0ec8513](https://gitlab.com/commonground/core/design-system/commit/0ec8513e21310481661916e3c4bd24664cec33e7))
* **button:** add style for the focus state ([5147114](https://gitlab.com/commonground/core/design-system/commit/5147114ea973e1c7c1c44097b2a9ef83772fc4d6))

# [11.2.0](https://gitlab.com/commonground/core/design-system/compare/v11.1.0...v11.2.0) (2020-04-20)


### Features

* **spinner:** add component ([ce7b96c](https://gitlab.com/commonground/core/design-system/commit/ce7b96c2f644b72cb9fe6b0a73d22eb036eaedde))

# [11.1.0](https://gitlab.com/commonground/core/design-system/compare/v11.0.1...v11.1.0) (2020-04-17)


### Features

* **button:** add danger variant ([fc06954](https://gitlab.com/commonground/core/design-system/commit/fc069541df6a572ee85751f94c587a8fb8f4bd83))

## [11.0.1](https://gitlab.com/commonground/core/design-system/compare/v11.0.0...v11.0.1) (2020-04-17)


### Bug Fixes

* display radio label above the options ([01e987d](https://gitlab.com/commonground/core/design-system/commit/01e987d97e89be75abf58d4ea51feb240fb2a174))

# [11.0.0](https://gitlab.com/commonground/core/design-system/compare/v10.1.3...v11.0.0) (2020-04-17)


### Features

* **alert:** add alert role ([f9c129f](https://gitlab.com/commonground/core/design-system/commit/f9c129f0ead20b61a69f69acc1ca156564d743cd))
* **checkbox:** add component ([0403585](https://gitlab.com/commonground/core/design-system/commit/04035852cfe757bc98124e7ec31155e30ce4cde7))
* **input:** add pointer cursor for radio & checkboxes ([9c8e765](https://gitlab.com/commonground/core/design-system/commit/9c8e7652e402a8473f56b66e75304bede2fa1932))
* **input:** add styling for focus state ([54cdfa6](https://gitlab.com/commonground/core/design-system/commit/54cdfa69fbc512f79354e6e5ec922e284a6c6389))
* **input:** add styling for text inputs ([9af8a85](https://gitlab.com/commonground/core/design-system/commit/9af8a856057bd9b88e8e830278295c0d274c05ec))
* **input:** add support for different sizes ([a066ebb](https://gitlab.com/commonground/core/design-system/commit/a066ebb9c28d1b34511a2f610df09875e75376ff))
* **radio:** add component ([68df632](https://gitlab.com/commonground/core/design-system/commit/68df632d81cf3d1747bc2f7c6ab1c9cdace5b143))
* **textinput:** add component ([7147d45](https://gitlab.com/commonground/core/design-system/commit/7147d458de1ae6e414147510a9c3125e2f5f87d5))


### Reverts

* remove Input & LabelWithInput components ([1207faa](https://gitlab.com/commonground/core/design-system/commit/1207faaabb02525e7ad76a294c9dd76b48c3fd80))


### BREAKING CHANGES

* these components are being removed in favour of new input components (with a new API) which are coming up

## [10.1.3](https://gitlab.com/commonground/core/design-system/compare/v10.1.2...v10.1.3) (2020-04-09)


### Bug Fixes

* styled-components should not be bundled as a dependency ([c6ae261](https://gitlab.com/commonground/core/design-system/commit/c6ae261a31b612f43da39b869d8a239dddac4be5))

## [10.1.2](https://gitlab.com/commonground/core/design-system/compare/v10.1.1...v10.1.2) (2020-04-09)


### Bug Fixes

* generate the pages only on a release ([11759f7](https://gitlab.com/commonground/core/design-system/commit/11759f712d02cb5656cc779daab8353a514e6f83))

## [10.1.1](https://gitlab.com/commonground/core/design-system/compare/v10.1.0...v10.1.1) (2020-04-09)


### Bug Fixes

* **drawer:** only clicking outside the drawer should close it ([c7d9135](https://gitlab.com/commonground/core/design-system/commit/c7d91357b14eb8fc0634213bf1423f84294d087c))

# [10.1.0](https://gitlab.com/commonground/core/design-system/compare/v10.0.5...v10.1.0) (2020-04-08)


### Features

* **input:** introduce component with type checkbox and radio ([1b8401e](https://gitlab.com/commonground/core/design-system/commit/1b8401ec94c7b83f9e1e6535d269c8420c435926))

## [10.0.5](https://gitlab.com/commonground/core/design-system/compare/v10.0.4...v10.0.5) (2020-04-08)


### Bug Fixes

* dependencies ([4cf41fa](https://gitlab.com/commonground/core/design-system/commit/4cf41fa5c9254885b957d9af581f25cf252e1fb9))

## [10.0.4](https://gitlab.com/commonground/core/design-system/compare/v10.0.3...v10.0.4) (2020-04-06)


### Bug Fixes

* **button:** define missing proptypes ([1104a55](https://gitlab.com/commonground/core/design-system/commit/1104a550e24d1dddd1df38c6e7cc609fee615084))
* **nlxlogo:** specify default props ([95c8d1b](https://gitlab.com/commonground/core/design-system/commit/95c8d1b18c3c21d536ac42936e4804bbb26a33cd))

## [10.0.3](https://gitlab.com/commonground/core/design-system/compare/v10.0.2...v10.0.3) (2020-03-31)


### Bug Fixes

* **globalstyles:** update text color of small to use colorTextLabel ([d42ed22](https://gitlab.com/commonground/core/design-system/commit/d42ed222bd0ad1e58d0ee2d1ad2301a47d7d63e4))

## [10.0.2](https://gitlab.com/commonground/core/design-system/compare/v10.0.1...v10.0.2) (2020-03-30)


### Bug Fixes

* **GlobalStyles:** update text color of small to use colorTextLabel ([659820d](https://gitlab.com/commonground/core/design-system/commit/659820d0f7686c1f0f8d508feb4bff06f2a1f3c5))

## [10.0.1](https://gitlab.com/commonground/core/design-system/compare/v10.0.0...v10.0.1) (2020-03-27)


### Bug Fixes

* **Alert:** export component ([e7ae0f3](https://gitlab.com/commonground/core/design-system/commit/e7ae0f35a416013d1d6c2d0d686b1ad25e32f198))

# [10.0.0](https://gitlab.com/commonground/core/design-system/compare/v9.1.2...v10.0.0) (2020-03-27)


### Features

* **Alert:** add component ([a7f41cc](https://gitlab.com/commonground/core/design-system/commit/a7f41cc9fa5c03ca208e3439637bb4f5e14fbfc8))


### BREAKING CHANGES

* **Alert:** the previously known 'alert colors' have been removed and are now known as sub-colors

**Before:**

```
// Alerts
colorAlertInfo
colorAlertInfoLight
colorAlertWarning
colorAlertWarningLight
colorAlertError
colorAlertErrorLight
colorAlertSuccess
colorAlertSuccessLight
```

**After:**

```
// Sub-colors
colorInfo
colorInfoLight
colorWarning
colorWarningLight
colorError
colorErrorLight
colorSuccess
colorSuccessLight
```

## [9.1.2](https://gitlab.com/commonground/core/design-system/compare/v9.1.1...v9.1.2) (2020-03-26)


### Bug Fixes

* secondary button hover color ([72a432e](https://gitlab.com/commonground/core/design-system/commit/72a432e5c344e4b92cb7f5faa44ca96ee772a97c))

## [9.1.1](https://gitlab.com/commonground/core/design-system/compare/v9.1.0...v9.1.1) (2020-03-26)


### Bug Fixes

* **Drawer:** design updates ([6b32ba5](https://gitlab.com/commonground/core/design-system/commit/6b32ba5e7a025c57e609f1baf6ab50b92da613df))

# [9.1.0](https://gitlab.com/commonground/core/design-system/compare/v9.0.2...v9.1.0) (2020-03-26)


### Features

* **Drawer:** add component ([5a380dc](https://gitlab.com/commonground/core/design-system/commit/5a380dc782cf251c522ebe5fed48e5adbef02389)), closes [commonground/nlx/nlx#897](https://gitlab.com/commonground/nlx/nlx/issues/897)

## [9.0.2](https://gitlab.com/commonground/core/design-system/compare/v9.0.1...v9.0.2) (2020-03-24)


### Bug Fixes

* **button:** hover ([671dff8](https://gitlab.com/commonground/core/design-system/commit/671dff871b6d4a73a884c75a299ba9792b145989))

## [9.0.1](https://gitlab.com/commonground/core/design-system/compare/v9.0.0...v9.0.1) (2020-03-24)


### Bug Fixes

* **deps:** update dependency polished to v3.5.1 ([389afc4](https://gitlab.com/commonground/core/design-system/commit/389afc4eb520b18f8981f7118a84a1adf763fe9d))

# [9.0.0](https://gitlab.com/commonground/core/design-system/compare/v8.4.0...v9.0.0) (2020-03-20)


### deps

* upgrade dependency polished ([c27b0e7](https://gitlab.com/commonground/core/design-system/commit/c27b0e715ab73f13e06fafba24293d04827f6efa))


### BREAKING CHANGES

* triggering a breaking change here, since we forgot to do so in the previous MR. The structure & naming of the design tokens have been updated.

Previous MR: https://gitlab.com/commonground/core/design-system/-/merge_requests/156

# [8.4.0](https://gitlab.com/commonground/core/design-system/compare/v8.3.1...v8.4.0) (2020-03-19)


### Bug Fixes

* **Button:** update cursor to show pointer ([6a7f986](https://gitlab.com/commonground/core/design-system/commit/6a7f986122797132f78db8e4f894c79d0ba4ca67))


### Features

* **Button:** implement styling for disabled state ([bac08fc](https://gitlab.com/commonground/core/design-system/commit/bac08fc31ed309c877705db889651684ef61b90f))

## [8.3.1](https://gitlab.com/commonground/core/design-system/compare/v8.3.0...v8.3.1) (2020-03-16)


### Bug Fixes

* move polished from devDependencies to dependencies ([a801ea7](https://gitlab.com/commonground/core/design-system/commit/a801ea7734454a21cc6ba75cc7a50333f3482428))

# [8.3.0](https://gitlab.com/commonground/core/design-system/compare/v8.2.2...v8.3.0) (2020-03-12)


### Features

* **Button:** added component ([d381de2](https://gitlab.com/commonground/core/design-system/commit/d381de2bb379fd33d5abeb1ee7c691c22ef83081))

## [8.2.2](https://gitlab.com/commonground/core/design-system/compare/v8.2.1...v8.2.2) (2020-03-12)


### Bug Fixes

* **GlobalStyles:** do not use styled-components/macro ([272d55f](https://gitlab.com/commonground/core/design-system/commit/272d55f09c9d435caac0246c56d3329fd7c07f8c))

## [8.2.1](https://gitlab.com/commonground/core/design-system/compare/v8.2.0...v8.2.1) (2020-03-12)


### Bug Fixes

* **GlobalStyles:** export GlobalStyles component ([0fba713](https://gitlab.com/commonground/core/design-system/commit/0fba7132a60179df58e40bca7abdcc2f60107b55))

# [8.2.0](https://gitlab.com/commonground/core/design-system/compare/v8.1.0...v8.2.0) (2020-03-11)


### Features

* rework ordering of tokens ([9e6352b](https://gitlab.com/commonground/core/design-system/commit/9e6352be1782e73de05fe21733be36c12ae6e2b6))
* **GlobalStyles:** implement GlobalStyles component with a default theme ([9dfe5f4](https://gitlab.com/commonground/core/design-system/commit/9dfe5f423e34c66f006d6f53481f74b816cece5f))
* **VersionLogger:** remove component ([317abf1](https://gitlab.com/commonground/core/design-system/commit/317abf1f8c142eec4b63840e6fff30dcafb51a11))

# [8.1.0](https://gitlab.com/commonground/core/design-system/compare/v8.0.0...v8.1.0) (2020-03-10)


### Features

* **NLXLogo:** add white variant ([919251b](https://gitlab.com/commonground/core/design-system/commit/919251bc81cc388f0472fc3b320d8d83e9c3b2de))

# [8.0.0](https://gitlab.com/commonground/core/design-system/compare/v7.2.1...v8.0.0) (2020-03-05)


### chore

* **Card:** removed component nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([2d66264](https://gitlab.com/commonground/core/design-system/commit/2d662648f2d500de829477cba1354047e8c00fb3))
* **Pagination:** removed component nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([ad19374](https://gitlab.com/commonground/core/design-system/commit/ad19374953bdc0183fa8fbbe49caf0002e025c44))
* **Spinner:** removed component nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([5fd53bd](https://gitlab.com/commonground/core/design-system/commit/5fd53bd7432d50a34fb6d8ae6ebac6dcf46504bc))
* **Table:** removed component nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([774583d](https://gitlab.com/commonground/core/design-system/commit/774583d9eef9023bd81b861a058e625e7bfde8f5))
* removed Container, Header, IconButton, Navigation & NLXNavbar components nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([7fad8cf](https://gitlab.com/commonground/core/design-system/commit/7fad8cfd7ce33ed971f7e48b2125979a8c740483))
* removed Search & SearchIcon components nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([a5a43dc](https://gitlab.com/commonground/core/design-system/commit/a5a43dc2f3a5bd58fb4090d3554856956064bac8))


### Features

* added tests for logos ([7777e2f](https://gitlab.com/commonground/core/design-system/commit/7777e2f198809c9313c5f7086b0004340d5c67bd))
* created RTL tests for VersionLogger ([b789ea0](https://gitlab.com/commonground/core/design-system/commit/b789ea01af7df7b2373a516961270d326b9c2f51))
* deprecated Pagination ([9e349cb](https://gitlab.com/commonground/core/design-system/commit/9e349cb061ff48772925ef48350ef68993d73d58))
* deprecation notice for Pagination ([fcd6602](https://gitlab.com/commonground/core/design-system/commit/fcd6602bf025c923944e6bc88278e007e3c44aa3))
* Pagination: tests rewritten in testing-library ([13d93ea](https://gitlab.com/commonground/core/design-system/commit/13d93eab3054fe3f4a19c4b8e10bbbc373bb8532))


### BREAKING CHANGES

* **Pagination:** the component has been extracted to its only consumer
* **Table:** the components have been extracted into its consumers
* **Card:** the components have been extracted into its consumers
* the components have been extracted into its only consumer - the Insight UI
* the components have been extracted into its only consumer - the Insight UI
* **Spinner:** the Spinner component has been extracted into its only consumer - the Directory UI

## [7.2.1](https://gitlab.com/commonground/cg-design-system/compare/v7.2.0...v7.2.1) (2019-10-25)


### Bug Fixes

* **gitlab-ci:** replace alpine with full docker base image ([a316291](https://gitlab.com/commonground/cg-design-system/commit/a31629115a026fdfa6aca9c5bec8522040617e97))

# [7.2.0](https://gitlab.com/commonground/cg-design-system/compare/v7.1.0...v7.2.0) (2019-09-05)


### Features

* **CGLogo:** add Common Ground logo ([1f8f339](https://gitlab.com/commonground/cg-design-system/commit/1f8f339))

# [7.1.0](https://gitlab.com/commonground/cg-design-system/compare/v7.0.0...v7.1.0) (2019-06-13)


### Features

* **VersionLogger:** add new component to log the tag from `/version.json` in the application this component is used in. ([1dd7a4e](https://gitlab.com/commonground/cg-design-system/commit/1dd7a4e))

# [7.0.0](https://gitlab.com/commonground/cg-design-system/compare/v6.0.0...v7.0.0) (2019-06-11)


### Features

* **Pagination:** rework Pagination props to perform the calculation for the amount of pages internally ([2de2a0c](https://gitlab.com/commonground/cg-design-system/commit/2de2a0c))


### BREAKING CHANGES

* **Pagination:** the amountOfPages property has been replaced by the
totalRows and rowsPerPage properties. Both are required.

# [6.0.0](https://gitlab.com/commonground/cg-design-system/compare/v5.1.0...v6.0.0) (2019-06-07)


### Bug Fixes

* **Pagination:** make previous and next button accessible ([04df6a9](https://gitlab.com/commonground/cg-design-system/commit/04df6a9))
* **Pagination:** require currentPage to be an integer ([44b0b11](https://gitlab.com/commonground/cg-design-system/commit/44b0b11))


### Features

* **Pagination:** increase spacing around input field ([8de8319](https://gitlab.com/commonground/cg-design-system/commit/8de8319))


### BREAKING CHANGES

* **Pagination:** before, it would be possible to provide a string as the
currentPage. This would lead to unexpected results. Eg. when passing '1'
as the currentPage, the onPageChangeHandler would pass '11' as the next
page. This fix makes sure the sum for the next page will output an
appropriate result.

# [5.1.0](https://gitlab.com/commonground/cg-design-system/compare/v5.0.0...v5.1.0) (2019-05-23)


### Bug Fixes

* **Pagination:** clicking the next button should trigger the ([78960aa](https://gitlab.com/commonground/cg-design-system/commit/78960aa))


### Features

* **Pagination:** added min and max attributes to the page input ([83db50b](https://gitlab.com/commonground/cg-design-system/commit/83db50b))

# [5.0.0](https://gitlab.com/commonground/cg-design-system/compare/v4.1.2...v5.0.0) (2019-05-08)


### Features

* **Search:** enable passing any props for the input field ([56d30b9](https://gitlab.com/commonground/cg-design-system/commit/56d30b9))


### BREAKING CHANGES

* **Search:** Search now takes an object for the input element
properties. The `inputId` and `inputName` property should be moved to
the `inputProps` object as `id` and `name` properties.

## [4.1.2](https://gitlab.com/commonground/cg-design-system/compare/v4.1.1...v4.1.2) (2019-05-03)


### Bug Fixes

* **ui:** increase contrast for active nav item ([ef9fa95](https://gitlab.com/commonground/cg-design-system/commit/ef9fa95))

## [4.1.1](https://gitlab.com/commonground/cg-design-system/compare/v4.1.0...v4.1.1) (2019-05-03)


### Bug Fixes

* release ([81d8393](https://gitlab.com/commonground/cg-design-system/commit/81d8393))

# [4.1.0](https://gitlab.com/commonground/cg-design-system/compare/v4.0.0...v4.1.0) (2019-05-02)


### Features

* **IconButton:** export component + add stories ([b718b0b](https://gitlab.com/commonground/cg-design-system/commit/b718b0b))
* **Table:** set background color to white for the BodyCell component ([c1fb4ce](https://gitlab.com/commonground/cg-design-system/commit/c1fb4ce))

# [4.0.0](https://gitlab.com/commonground/cg-design-system/compare/v3.1.0...v4.0.0) (2019-04-25)


### Bug Fixes

* **Navbar:** remove margin-right: auto; styling property ([3561488](https://gitlab.com/commonground/cg-design-system/commit/3561488))


### Features

* **Search:** enable setting the name and id attribute for the input field ([62c5a5d](https://gitlab.com/commonground/cg-design-system/commit/62c5a5d))


### BREAKING CHANGES

* **Navbar:** this property was causing issues for DON. The Navigation
component inside a flexbox element with justify-content was not taking
up the expected space, because of it's margin-right: auto; setting.

# [3.1.0](https://gitlab.com/commonground/cg-design-system/compare/v3.0.0...v3.1.0) (2019-04-25)


### Features

* **Pagination:** implement Pagination component ([cb9de5e](https://gitlab.com/commonground/cg-design-system/commit/cb9de5e))

# [3.0.0](https://gitlab.com/commonground/cg-design-system/compare/v2.0.0...v3.0.0) (2019-04-19)


### chore

* trigger new release ([80abe69](https://gitlab.com/commonground/cg-design-system/commit/80abe69))


### BREAKING CHANGES

* previous release (v2.0.0) was already tagged in GitLab.
This caused the deployment to fail.

# [2.0.0](https://gitlab.com/commonground/cg-design-system/compare/v1.0.3...v2.0.0) (2019-04-19)


### chore

* **Spinner:** re-name component files to index.js ([f7c6040](https://gitlab.com/commonground/cg-design-system/commit/f7c6040))
* rework Navbar into separate components + add NLXNavbar ([d60445e](https://gitlab.com/commonground/cg-design-system/commit/d60445e))


### Features

* **GitLabLogo:** add GitLabLogo as component ([a388ab5](https://gitlab.com/commonground/cg-design-system/commit/a388ab5))
* **Navbar:** add Navbar as component ([4eb3ace](https://gitlab.com/commonground/cg-design-system/commit/4eb3ace))
* **Navbar:** enable customizing the navigation URLs ([9e0905c](https://gitlab.com/commonground/cg-design-system/commit/9e0905c))
* **Navbar:** pass down props to its container ([abdd736](https://gitlab.com/commonground/cg-design-system/commit/abdd736))
* **NLXLogo:** add NLXLogo as component ([48dc435](https://gitlab.com/commonground/cg-design-system/commit/48dc435))
* **Search:** add Search component ([a05dfc0](https://gitlab.com/commonground/cg-design-system/commit/a05dfc0))
* **Search:** enable custom placeholder ([3e90b8c](https://gitlab.com/commonground/cg-design-system/commit/3e90b8c))
* **Table:** add Table as component ([0825a7c](https://gitlab.com/commonground/cg-design-system/commit/0825a7c))


### BREAKING CHANGES

* Navbar has been renamed to NLXNavbar and requires
Navbar.Item components as children.
* **Navbar:** URLs must be passed to the Navbar component
* **Spinner:** filenames have been updated. Import of the component
files should be updated.

## [1.0.3](https://gitlab.com/commonground/cg-design-system/compare/v1.0.2...v1.0.3) (2019-01-23)


### Bug Fixes

* **README:** prefix relative markdown links with ./ ([ede4110](https://gitlab.com/commonground/cg-design-system/commit/ede4110)), closes [#1](https://gitlab.com/commonground/cg-design-system/issues/1)

## [1.0.2](https://gitlab.com/commonground/cg-design-system/compare/v1.0.1...v1.0.2) (2019-01-23)


### Bug Fixes

* **README:** update Git URL to work with README links + rewrite links ([75225ab](https://gitlab.com/commonground/cg-design-system/commit/75225ab)), closes [#1](https://gitlab.com/commonground/cg-design-system/issues/1)

## [1.0.1](https://gitlab.com/commonground/cg-design-system/compare/v1.0.0...v1.0.1) (2019-01-23)


### Bug Fixes

* **README:** include docs in NPM package ([e30557e](https://gitlab.com/commonground/cg-design-system/commit/e30557e)), closes [#1](https://gitlab.com/commonground/cg-design-system/issues/1)

# 1.0.0 (2019-01-17)


### Bug Fixes

* **docs:** reword release process ([810dd89](https://gitlab.com/commonground/cg-design-system/commit/810dd89))
* use cross-env for Windows compatibility ([932d3a6](https://gitlab.com/commonground/cg-design-system/commit/932d3a6))


### chore

* fix publishing npm package by enabling unsafe-perm for rm -rf ([28b7640](https://gitlab.com/commonground/cg-design-system/commit/28b7640))
* install missing dependency [@semantic-release](https://gitlab.com/semantic-release)/changelog ([f3d6fad](https://gitlab.com/commonground/cg-design-system/commit/f3d6fad))
* release transpiled assets ([e5475c8](https://gitlab.com/commonground/cg-design-system/commit/e5475c8))
* release transpiled assets ([0960cdc](https://gitlab.com/commonground/cg-design-system/commit/0960cdc))
* replace [@semantic-release](https://gitlab.com/semantic-release)/gitlab-config with [@semantic-release](https://gitlab.com/semantic-release)/gitlab ([95a7626](https://gitlab.com/commonground/cg-design-system/commit/95a7626))
* replace prepublish with prebublishOnly ([d6af0e0](https://gitlab.com/commonground/cg-design-system/commit/d6af0e0)), closes [/github.com/npm/npm/issues/10074#issue-112707857](https://gitlab.com//github.com/npm/npm/issues/10074/issues/issue-112707857)
* replace rm -rf with rimraf ([28f2659](https://gitlab.com/commonground/cg-design-system/commit/28f2659))
* revert previous release ([3adf243](https://gitlab.com/commonground/cg-design-system/commit/3adf243))


### Documentation

* **readme:** remove whitespace ([e4b93da](https://gitlab.com/commonground/cg-design-system/commit/e4b93da))


### Features

* **Spinner:** expose Spinner component by setting NPM's main file ([8862c1a](https://gitlab.com/commonground/cg-design-system/commit/8862c1a))


### BREAKING CHANGES

* https://github.com/npm/npm/issues/2984#issuecomment-67626518
* prepublish is deprecated and also ran when installing
* prevent NPM error 'cannot run in wd'
* ignore test files and snapshots
* 
* trigger release to prepare for v1
* trigger release
* trigger release
* **readme:** Triggering a new release.
