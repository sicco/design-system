Development Setup
=================

Installation

    npm install
    
Run tests

    npm test
    
Run tests in watch mode

    npm test -- --watch
    
Run tests with code coverage report
    
    npm test -- --coverage
