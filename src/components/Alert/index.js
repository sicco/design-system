// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { oneOf, string, node } from 'prop-types'
import { Container, Content, Title } from './index.styles'

/**
 * Use `Alert` to provide local feedback, relative to another element.
 *
 * For global feedback, you may want to use a `Toaster`.
 */
const Alert = ({ title, children, ...props }) => (
  <Container role="alert" {...props}>
    {title && title.length > 1 ? (
      <Title data-testid="title">{title}</Title>
    ) : null}
    <Content data-testid="content">{children}</Content>
  </Container>
)

Alert.propTypes = {
  children: node,
  title: string,
  variant: oneOf(['info', 'warning', 'success', 'error']),
}

Alert.defaultProps = {
  variant: 'info',
}

export default Alert
