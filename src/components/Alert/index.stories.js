// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import Alert from './index'

export default {
  title: 'Components/Alert',
  parameters: {
    componentSubtitle: 'Localised notifications',
  },
  component: Alert,
}

export const allAlerts = () => (
  <>
    <Alert title="Info (default)" variant="info">
      Info message
    </Alert>
    <br />
    <Alert title="Success" variant="success">
      Success message
    </Alert>
    <br />
    <Alert title="Warning" variant="warning">
      Warning message
    </Alert>
    <br />
    <Alert title="Error" variant="error">
      Error message
    </Alert>
  </>
)

export const info = () => (
  <Alert variant="info" title="Title">
    Content
  </Alert>
)

export const success = () => (
  <Alert variant="success" title="Title">
    Content
  </Alert>
)

export const warning = () => (
  <Alert variant="warning" title="Title">
    Content
  </Alert>
)

export const error = () => (
  <Alert variant="error" title="Title">
    Content
  </Alert>
)
