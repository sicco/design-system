// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { render } from '@testing-library/react'

import TestThemeProvider from '../../themes/TestThemeProvider'
import Alert from './index'

describe('Alert', () => {
  it('should exist', () => {
    const { getByTestId } = render(
      <TestThemeProvider>
        <Alert title="My First Alert" data-testid="alert">
          Hello, World!
        </Alert>
      </TestThemeProvider>,
    )

    expect(getByTestId('alert')).toBeTruthy()
    expect(getByTestId('title').textContent).toBe('My First Alert')
    expect(getByTestId('content').textContent).toBe('Hello, World!')
  })
})
