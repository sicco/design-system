// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { css } from 'styled-components'

export default (p) =>
  p.disabled
    ? css`
        cursor: not-allowed;
        color: ${p.theme.colorTextButtonDangerDisabled};
        background-color: ${p.theme.colorBackgroundButtonDangerDisabled};

        &:hover,
        &:focus {
          color: ${p.theme.colorTextButtonDangerDisabled};
          background-color: ${p.theme.colorBackgroundButtonDangerDisabled};
        }
      `
    : css`
        color: ${p.theme.colorTextButtonDanger};
        background-color: ${p.theme.colorBackgroundButtonDanger};

        &:hover,
        &:focus {
          color: ${p.theme.colorTextButtonDanger};
          background: ${p.theme.colorBackgroundButtonDangerHover};
        }
      `
