// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import styled from 'styled-components'
import { rgba } from 'polished'

import { bool, oneOf } from 'prop-types'
import getPrimaryStyles from './primary.style'
import getSecondaryStyles from './secondary.style'
import getDangerStyles from './danger.style'

const INNER_BORDER_WIDTH = '2px'

const StyledButton = styled.button`
  display: inline-flex;
  align-items: center;
  padding: calc(${(p) => p.theme.tokens.spacing05} - ${INNER_BORDER_WIDTH});
  border: ${INNER_BORDER_WIDTH} solid transparent;
  line-height: ${(p) => p.theme.tokens.fontSizeMedium};
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  font-weight: ${(p) => p.theme.tokens.fontWeightSemiBold};
  text-decoration: none;
  cursor: pointer;
  position: relative;

  ${(p) => {
    switch (p.variant) {
      case 'danger':
        return getDangerStyles(p)

      case 'secondary':
        return getSecondaryStyles(p)

      case 'primary':
      default:
        return getPrimaryStyles(p)
    }
  }}

  &:after {
    background-color: ${(p) => rgba(p.theme.tokens.colorPaletteGray900, 0.25)};
    content: '';
    position: absolute;
    height: 3px;
    bottom: -${INNER_BORDER_WIDTH};
    left: -${INNER_BORDER_WIDTH};
    right: -${INNER_BORDER_WIDTH};
  }

  &:focus {
    border: ${INNER_BORDER_WIDTH} solid ${(p) => p.theme.tokens.colorBackground};
    outline: ${INNER_BORDER_WIDTH} solid ${(p) => p.theme.tokens.colorFocus};

    &:after {
      display: none;
    }
  }
`

/**
 * Use `Button` for both buttons and anchor elements.
 */
const Button = (props) => <StyledButton {...props} />

Button.propTypes = {
  variant: oneOf(['primary', 'secondary', 'danger']),
  disabled: bool,
}

Button.defaultProps = {
  variant: 'primary',
  disabled: false,
}

export default Button
