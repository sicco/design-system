// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'

import { Link } from 'react-router-dom'
import { MemoryRouter as Router } from 'react-router'
import Button from './index'

export default {
  title: 'Components/Button',
  parameters: {
    componentSubtitle:
      'Button component which can be used as button, anchor and React Router Link',
  },
  component: Button,
}

export const allButtons = () => (
  <>
    <Button variant="primary">Primary (default)</Button>
    <br />
    <br />
    <Button variant="secondary">Secondary</Button>
    <br />
    <br />
    <Button variant="danger">Danger</Button>
    <br />
    <br />
    <Button disabled>Disabled</Button>
  </>
)

export const primary = () => (
  <>
    <Button variant="primary">Primary (default)</Button>
    <br />
    <br />
    <Button variant="primary" disabled>
      Primary (disabled)
    </Button>
  </>
)

export const secondary = () => (
  <>
    <Button variant="secondary">Secondary</Button>
    <br />
    <br />
    <Button variant="secondary" disabled>
      Secondary (disabled)
    </Button>
  </>
)

export const danger = () => (
  <>
    <Button variant="danger">Danger</Button>
    <br />
    <br />
    <Button variant="danger" disabled>
      Danger (disabled)
    </Button>
  </>
)

export const disabled = () => <Button disabled>Disabled</Button>

export const asAnchor = () => (
  <Button as="a" href="https://www.duck.com">
    Go to DuckDuckGo.com
  </Button>
)

export const withReactRouter = () => (
  <Router>
    <Button as={Link} to="/">
      Navigate to root (/)
    </Button>
  </Router>
)
