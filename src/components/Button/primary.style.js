// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { css } from 'styled-components'

export default (p) =>
  p.disabled
    ? css`
        cursor: not-allowed;
        color: ${p.theme.colorTextButtonPrimaryDisabled};
        background-color: ${p.theme.colorBackgroundButtonPrimaryDisabled};

        &:hover,
        &:focus {
          color: ${p.theme.colorTextButtonPrimaryDisabled};
          background-color: ${p.theme.colorBackgroundButtonPrimaryDisabled};
        }
      `
    : css`
        color: ${p.theme.colorTextButtonPrimary};
        background-color: ${p.theme.colorBackgroundButtonPrimary};

        &:hover,
        &:focus {
          color: ${p.theme.colorTextButtonPrimary};
          background: ${p.theme.colorBackgroundButtonPrimaryHover};
        }
      `
