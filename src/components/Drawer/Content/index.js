// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { node } from 'prop-types'
import { StyledContent } from './index.styles'

const Content = ({ children, ...props }) => (
  <StyledContent {...props}>{children}</StyledContent>
)

Content.propTypes = {
  children: node,
}

export default Content
