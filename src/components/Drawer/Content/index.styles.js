// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import styled from 'styled-components'

export const StyledContent = styled.div`
  margin-top: 2.5rem;
`
