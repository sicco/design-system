// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { string } from 'prop-types'
import { DrawerContext } from '../index'
import CloseButton from '../CloseButton'
import { StyledHeader, StyledTitle } from './index.styles'

const Header = ({ title, closeButtonLabel, ...props }) => (
  <DrawerContext.Consumer>
    {({ close }) => {
      return (
        <StyledHeader {...props}>
          <StyledTitle>{title}</StyledTitle>
          <CloseButton
            onClick={close}
            data-testid="close-button"
            aria-label={closeButtonLabel}
            title={closeButtonLabel}
          />
        </StyledHeader>
      )
    }}
  </DrawerContext.Consumer>
)

Header.propTypes = {
  title: string,
  closeButtonLabel: string,
}

Header.defaultProps = {
  closeButtonLabel: 'Close',
}

export default Header
