// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { createContext, useState, useEffect, useCallback } from 'react'
import { bool, func, node } from 'prop-types'
import { CSSTransition } from 'react-transition-group'
import { FocusOn } from 'react-focus-on'

import Header from './Header'
import Content from './Content'

import {
  StyledMask,
  StyledContainer,
  StyledDrawerContent,
} from './index.styles'
import CloseButton from './CloseButton'

const ESCAPE_KEY_CODE = 27
const ANIMATION_SPEED = 250

export const DrawerContext = createContext()

/**
 * Use `Drawer` to display an overlay providing more information.
 */
const Drawer = ({
  skipOpenAnimation,
  closeHandler,
  children,
  autoFocus,
  noMask,
  ...props
}) => {
  const [inProp, setInProp] = useState(!!skipOpenAnimation)

  const close = useCallback(() => {
    setInProp(false)
    setTimeout(closeHandler, ANIMATION_SPEED)
  }, [closeHandler])

  useEffect(() => {
    setInProp(true)
  }, [])

  // when `noMask` is true, we don't want the `FocusOn` functionality so we disabled it and
  // handle the escape key manually
  useEffect(() => {
    if (noMask) {
      const handleDocumentkeydownHandler = (event) => {
        if (event.keyCode === ESCAPE_KEY_CODE) {
          close()
        }
      }

      document.addEventListener('keydown', handleDocumentkeydownHandler)
      return () =>
        document.removeEventListener('keydown', handleDocumentkeydownHandler)
    }
  }, [noMask, close])

  return (
    <StyledContainer {...props}>
      {!noMask ? (
        <CSSTransition
          in={inProp}
          appear={skipOpenAnimation}
          timeout={0}
          unmountOnExit
          classNames="fade-in"
        >
          <StyledMask />
        </CSSTransition>
      ) : null}

      <FocusOn
        enabled={!noMask && inProp}
        onClickOutside={!noMask ? close : () => {}}
        onEscapeKey={close}
        autoFocus={autoFocus}
      >
        <CSSTransition
          in={inProp}
          appear={skipOpenAnimation}
          timeout={{
            appear: 0,
            enter: ANIMATION_SPEED,
            exit: ANIMATION_SPEED,
          }}
          unmountOnExit
          classNames="slide-in"
        >
          <StyledDrawerContent data-testid="content">
            <DrawerContext.Provider value={{ close }}>
              {children}
            </DrawerContext.Provider>
          </StyledDrawerContent>
        </CSSTransition>
      </FocusOn>
    </StyledContainer>
  )
}

Drawer.propTypes = {
  skipOpenAnimation: bool,
  closeHandler: func.isRequired,
  children: node.isRequired,
  autoFocus: bool,
  noMask: bool,
}

Drawer.defaultProps = {
  skipOpenAnimation: false,
  autoFocus: false,
  noMask: false,
}

Drawer.DrawerContext = DrawerContext
Drawer.Header = Header
Drawer.Content = Content
Drawer.CloseButton = CloseButton

export default Drawer
