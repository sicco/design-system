// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import mediaQueries from '../../mediaQueries'

const drawerWidth = '34.5rem'

export const StyledMask = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  opacity: 0.25;
  background: rgba(0, 0, 0, 0.6);
  transform: translateX(-100%);

  transition: opacity 220ms;

  &.fade-in-appear {
    opacity: 0.75;
  }

  &.fade-in-enter-done {
    transform: translateX(0);
    opacity: 0.75;
  }
`

export const StyledContainer = styled.div`
  z-index: 10;
`

export const StyledDrawerContent = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  padding: ${(p) => p.theme.tokens.spacing05};
  width: ${drawerWidth};
  max-width: 100%;
  overflow: auto;
  background-color: ${(p) => p.theme.colorBackgroundDrawer};
  box-shadow: 0 0 ${(p) => p.theme.tokens.spacing06} 0 rgba(0, 0, 0, 0.5);
  transform: translateX(${drawerWidth});

  ${mediaQueries.smUp`
    padding: ${(p) => p.theme.tokens.spacing08};
  `}

  &.slide-in-enter-active {
    transition: transform 250ms ease-out;
    transform: translateX(0);
  }

  &.slide-in-exit {
    transform: translateX(0);
  }

  &.slide-in-exit-active {
    transform: translateX(${drawerWidth});
    transition: transform 150ms ease-in;
  }

  &.slide-in-enter-done {
    transform: translateX(0);
  }
`
