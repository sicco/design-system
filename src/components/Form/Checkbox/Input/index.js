// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { bool } from 'prop-types'
import { StyledInput } from './index.styles'

const Input = (props) => <StyledInput {...props} />

Input.propTypes = {
  disabled: bool,
}

Input.defaultProps = {
  disabled: false,
}

export default Input
