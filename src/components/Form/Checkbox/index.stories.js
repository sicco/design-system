// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Formik, Form } from 'formik'

import { Fieldset, Legend } from '../index'
import Button from '../../Button'
import Checkbox from './index'

export default {
  title: 'Components/Form/Checkbox',
  parameters: {
    componentSubtitle:
      'Form component to work with checkboxes. Batteries included (label & input).',
  },
  component: Checkbox,
}

export const Intro = () => {
  return (
    <>
      <Formik
        initialValues={{ projects: [], pizza: false }}
        onSubmit={(values) => {
          alert(JSON.stringify(values))
        }}
      >
        <Form>
          <Fieldset>
            <Legend>Multiple options</Legend>
            <Checkbox name="projects" value="project-a">
              Project A
            </Checkbox>

            <Checkbox name="projects" value="project-b">
              Project B
            </Checkbox>
          </Fieldset>

          <Fieldset>
            <Legend>Single option</Legend>

            <Checkbox name="pizza">Do you like pizza?</Checkbox>
          </Fieldset>

          <Button type="submit">Submit</Button>
        </Form>
      </Formik>
    </>
  )
}

export const disabled = () => {
  return (
    <>
      <Formik initialValues={{ myCheckbox: true }} onSubmit={() => {}}>
        <Form>
          <Checkbox name="myCheckbox" disabled>
            Are you ok?
          </Checkbox>
        </Form>
      </Formik>
    </>
  )
}
