// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { bool, string, node } from 'prop-types'
import { Field, useField } from 'formik'
import { Label } from '../index'
import { StyledLabel } from './index.styles'
import Input from './Input'

const RadioGroup = ({ children, label, ...props }) => {
  return (
    <>
      <Label {...props}>{label}</Label>
      {children}
    </>
  )
}

RadioGroup.propTypes = {
  children: node.isRequired,
  label: string.isRequired,
}

RadioGroup.defaultProps = {
  disabled: false,
}

const Radio = ({ children, ...props }) => {
  const [field] = useField({ ...props, type: 'radio' })
  const { disabled } = props

  return (
    <StyledLabel disabled={disabled ? true : undefined}>
      <Field type="radio" as={Input} {...field} {...props} />
      {children}
    </StyledLabel>
  )
}

Radio.Group = RadioGroup

Radio.propTypes = {
  children: string.isRequired,
  value: string.isRequired,
  disabled: bool,
}

Radio.defaultProps = {
  disabled: false,
}

export default Radio
