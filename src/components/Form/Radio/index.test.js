// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { render } from '@testing-library/react'
import { Formik } from 'formik'
import TestThemeProvider from '../../../themes/TestThemeProvider'
import Radio from './index'

test('default ui', async () => {
  const { getByLabelText, getByText } = render(
    <TestThemeProvider>
      <Formik initialValues={{ option: 'option-a' }} onSubmit={() => {}}>
        <form data-testid="form">
          <Radio.Group label="Pick an option">
            <Radio name="option" value="option-a">
              Option A
            </Radio>
            <Radio name="option" value="option-b">
              Option B
            </Radio>
          </Radio.Group>
        </form>
      </Formik>
    </TestThemeProvider>,
  )

  // default UI
  expect(getByText('Pick an option')).toBeTruthy()
  expect(getByLabelText('Option A')).toBeTruthy()
  expect(getByLabelText('Option B')).toBeTruthy()
})
