// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { bool, oneOf } from 'prop-types'
import { StyledInput } from './index.styles'

const Input = (props) => <StyledInput {...props} />

Input.propTypes = {
  size: oneOf(['xs', 's', 'm', 'l', 'xl']),
  disabled: bool,
}

Input.defaultProps = {
  size: 'm',
  disabled: false,
}

export default Input
