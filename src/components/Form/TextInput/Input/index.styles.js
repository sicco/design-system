// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled, { css } from 'styled-components'

export const StyledInput = styled.input`
  background-color: ${(p) => p.theme.colorBackgroundInput};
  display: block;
  width: 100%;
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  font-family: 'Source Sans Pro', sans-serif;
  padding: ${(p) => p.theme.tokens.spacing04};
  color: ${(p) => p.theme.colorTextInputLabel};
  border: 1px solid ${(p) => p.theme.colorBorderInput};
  outline: none;
  line-height: ${(p) => p.theme.tokens.lineHeightText};
  margin-top: ${(p) => p.theme.tokens.spacing01};

  &:focus {
    padding: calc(${(p) => p.theme.tokens.spacing04} - 1px);
    border: 2px solid ${(p) => p.theme.colorBorderInputFocus};
  }

  &:placeholder {
    color: ${(p) => p.theme.colorTextInputPlaceholder};
  }

  &.invalid {
    padding: calc(${(p) => p.theme.tokens.spacing04} - 1px);
    border: 2px solid ${(p) => p.theme.colorBorderInputError};
  }

  ${(p) =>
    p.disabled
      ? css`
          background-color: ${(p) => p.theme.colorBackgroundInputDisabled};
          border-color: ${(p) => p.theme.colorBorderInputDisabled};
          cursor: not-allowed;
          color: ${(p) => p.theme.colorTextInputDisabled};
        `
      : null}

  ${(p) => {
    let width
    switch (p.size) {
      case 'xs':
        width = '5rem'
        break

      case 's':
        width = '10rem'
        break

      case 'm':
        width = '20rem'
        break

      case 'l':
        width = '30rem'
        break

      case 'xl':
        width = '46rem'
        break

      default:
        console.warn(
          `invalid size '${p.size}' provided. the supported values are xs, s, m, l and xl`,
        )
    }

    return css`
      width: ${width};
    `
  }}
`
