// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { bool, oneOf, string } from 'prop-types'
import { Field, useField } from 'formik'
import ErrorMessage from '../ErrorMessage'
import { StyledLabel } from './index.styles'
import Input from './Input'

const TextInput = ({ children, showError, ...props }) => {
  const [field, meta] = useField(props)
  const hasError =
    typeof showError === 'undefined' ? meta.error && meta.touched : showError

  const { disabled } = props

  return (
    <>
      <StyledLabel disabled={disabled ? true : undefined}>
        {children}
        <Field
          type="text"
          as={Input}
          {...field}
          {...props}
          className={hasError ? 'invalid' : null}
        />
      </StyledLabel>
      {hasError ? (
        <ErrorMessage data-testid={`error-${field.name}`}>
          {meta.error}
        </ErrorMessage>
      ) : null}
    </>
  )
}

TextInput.propTypes = {
  showError: bool,
  children: string.isRequired,
  name: string.isRequired,
  size: oneOf(['xs', 's', 'm', 'l', 'xl']),
  disabled: bool,
}

TextInput.defaultProps = {
  size: 'm',
  disabled: false,
}

export default TextInput
