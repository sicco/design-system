// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'

import Button from '../../Button'
import TextInput from './index'

export default {
  title: 'Components/Form/TextInput',
  parameters: {
    componentSubtitle:
      'Form component to work with text input. Batteries included (label, input & feedback).',
  },
  component: TextInput,
}

export const Intro = () => {
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('Required'),
  })

  return (
    <>
      <Formik
        validationSchema={validationSchema}
        initialValues={{ name: '' }}
        onSubmit={() => {}}
      >
        <Form>
          <TextInput name="name">My input</TextInput>
        </Form>
      </Formik>
    </>
  )
}

export const showError = () => {
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('Required'),
  })

  return (
    <>
      <p>
        Useful if you want to override the default validation logic. In this
        example we only trigger the error if the form has been submitted.
      </p>

      <Formik
        validationSchema={validationSchema}
        initialValues={{ name: '' }}
        onSubmit={() => {}}
      >
        {({ errors, submitCount }) => (
          <Form>
            <TextInput name="name" showError={errors.name && submitCount > 0}>
              My input
            </TextInput>

            <Button type="submit">Submit</Button>
          </Form>
        )}
      </Formik>
    </>
  )
}

export const size = () => {
  return (
    <>
      <Formik initialValues={{ name: '' }} onSubmit={() => {}}>
        <Form>
          <TextInput name="xs" size="xs">
            Extra small
          </TextInput>
          <TextInput name="s" size="s">
            Small
          </TextInput>
          <TextInput name="m" size="m">
            Medium (default)
          </TextInput>
          <TextInput name="l" size="l">
            Large
          </TextInput>
          <TextInput name="xl" size="xl">
            Extra large
          </TextInput>
        </Form>
      </Formik>
    </>
  )
}

export const disabled = () => {
  return (
    <>
      <Formik
        initialValues={{ name: '', street: 'My street 007' }}
        onSubmit={() => {}}
      >
        <Form>
          <TextInput name="disabled" disabled>
            Name
          </TextInput>

          <TextInput name="street" disabled>
            Street
          </TextInput>
        </Form>
      </Formik>
    </>
  )
}
