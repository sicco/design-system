// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled, { css } from 'styled-components'
import { Label } from '../index'

export const StyledLabel = styled(Label)`
  ${(p) =>
    p.disabled
      ? css`
          cursor: auto;
          color: ${p.theme.colorTextInputLabelDisabled};
          cursor: not-allowed;
        `
      : css`
          cursor: pointer;
        `}
`
