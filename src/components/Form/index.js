// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Label = styled.label`
  color: ${(p) => p.theme.colorTextInputLabel};
  display: block;
  word-break: keep-all;
  margin-bottom: ${(p) => p.theme.tokens.spacing01};
  margin-top: ${(p) => p.theme.tokens.spacing06};
  cursor: pointer;
`

export const Fieldset = styled.fieldset`
  border: 0 none;
  padding: 0 0 3rem 0;
`

export const Legend = styled.legend`
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
  margin: 0;
`
