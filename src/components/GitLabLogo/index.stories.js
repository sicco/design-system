// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import GitLabLogo from './index'

export default {
  title: 'Components/GitLabLogo',
  parameters: {
    componentSubtitle: 'The GitLab logo.',
  },
  component: GitLabLogo,
}

export const theLogo = () => <GitLabLogo style={{ height: '50px' }} />
