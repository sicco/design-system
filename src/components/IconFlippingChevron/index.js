// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { bool, number } from 'prop-types'
import styled, { css } from 'styled-components'

const IconChevronDown = ({ ...props }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width="24"
    height="24"
    {...props}
  >
    <path d="M12 13.172l4.95-4.95 1.414 1.414L12 16 5.636 9.636 7.05 8.222z" />
  </svg>
)

const IconChevron = ({ flipHorizontal, animationDuration, ...props }) => (
  <IconChevronDown {...props} />
)

IconChevron.propTypes = {
  flipHorizontal: bool,
  animationDuration: number,
}
IconChevron.defaultProps = {
  animationDuration: 150,
}

const IconFlippingChevron = styled(IconChevron)`
  fill: ${(p) => p.theme.colorText};
  transition: ${(p) => p.animationDuration}ms ease-in-out;

  ${(p) =>
    p.flipHorizontal
      ? css`
          transform: rotate(180deg);
        `
      : ''}
`

IconFlippingChevron.propTypes = {
  flipHorizontal: bool,
  animationDuration: number,
}

IconFlippingChevron.defaultProps = {
  animationDuration: 150,
}

export default IconFlippingChevron
