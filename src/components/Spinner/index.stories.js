// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import Spinner from './index'

export default {
  title: 'Components/Spinner',
  parameters: {
    componentSubtitle: 'Loader icon',
  },
  component: Spinner,
}

export const intro = () => <Spinner />
