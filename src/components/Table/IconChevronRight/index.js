// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'

const IconChevronRight = ({ ...props }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width="24px"
    height="24px"
    {...props}
  >
    <polygon
      fill="#9E9E9E"
      fillRule="nonzero"
      points="21.172 12 16.222 7.05 17.636 5.636 24 12 17.636 18.364 16.222 16.95"
    />
  </svg>
)

export default IconChevronRight
