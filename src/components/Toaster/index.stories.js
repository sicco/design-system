// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { useContext } from 'react'

import Button from '../Button'
import { ToasterProvider, ToasterContext, Toast } from './index'

export default {
  title: 'Components/Toaster',
  parameters: {
    componentSubtitle: 'Global notifications',
  },
  component: ToasterProvider,
}

export const Default = () => {
  // 1. Declare ToasterProvider somewhere high up the component tree
  return (
    <ToasterProvider rootSelector="#docs-root">
      <Toast body="It appears automatically!" />
      <DeeperDown />
    </ToasterProvider>
  )

  // 2. Deeper down, use the ToasterContext to show a Toast
  function DeeperDown() {
    const { showToast } = useContext(ToasterContext)

    const handleClick = () => {
      showToast({
        title: 'Successfully pressed button',
        body: 'Toast it like you mean it!',
        variant: 'success',
      })
    }

    return <Button onClick={handleClick}>Show a toaster</Button>
  }
}
