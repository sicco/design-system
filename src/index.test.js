// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as exportedModules from './index'

const components = [
  'darkTheme',
  'defaultTheme',
  'mediaQueries',

  'Alert',
  'Button',
  'CGLogo',
  'Checkbox',
  'Collapsible',
  'Drawer',
  'DrawerContext',
  'Fieldset',
  'GitLabLogo',
  'GlobalStyles',
  'IconFlippingChevron',
  'Radio',
  'TextInput',
  'Label',
  'Legend',
  'NLXLogo',
  'Spinner',
  'Table',
]

components.forEach((component) => {
  test(`component '${component}' is exported`, () => {
    expect(exportedModules[component]).toBeTruthy()
  })
})
