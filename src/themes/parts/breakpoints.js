// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export default {
  xs: 0, // mobile
  sm: 576, // mobile landscape
  md: 768, // tablets
  lg: 992, // Wide
}
