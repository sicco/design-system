// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export default {
  baseFontSize: '16px',

  spacing01: '0.125rem',
  spacing02: '0.25rem',
  spacing03: '0.5rem',
  spacing04: '0.75rem',
  spacing05: '1rem',
  spacing06: '1.5rem',
  spacing07: '2rem',
  spacing08: '2.5rem',
  spacing09: '3rem',
  spacing10: '4rem',
  spacing11: '5rem',
  spacing12: '6rem',

  lineHeightText: '150%',
  lineHeightHeading: '125%',

  fontWeightRegular: '500',
  fontWeightSemiBold: '600',
  fontWeightBold: '700',

  fontSizeSmall: '0.875rem',
  fontSizeMedium: '1rem',
  fontSizeLarge: '1.125rem',
  fontSizeXLarge: '1.5rem',
  fontSizeXXLarge: '2rem',
}
